# Zellij configuration with fish

if type -p zellij 2>&1 >/dev/null
    if status is-interactive && not test $ZELLIJ_SESSION_NAME && not test "$TERM" = dumb
        # Commands to run in interactive sessions can go here
        # set ZELLIJ_AUTO_ATTACH true
        set ZELLIJ_AUTO_EXIT true
        set -l ZJ_SESSIONS (zellij list-sessions -s)
        if test (count $ZJ_SESSIONS) -ge 1
            zellij attach -c (string split \n $ZJ_SESSIONS | fzf --print-query | tail -1)
            # zellij --layout session --session session-selector
        else
            zellij attach -c
        end
        if $ZELLIJ_AUTO_EXIT
            kill $fish_pid
        end
    end
end
